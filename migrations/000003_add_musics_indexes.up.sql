CREATE INDEX IF NOT EXISTS musics_title_idx ON musics USING GIN (to_tsvector('simple', title));
CREATE INDEX IF NOT EXISTS musics_genres_idx ON musics USING GIN (genres);