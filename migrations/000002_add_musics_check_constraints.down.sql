ALTER TABLE musics DROP CONSTRAINT IF EXISTS musics_runtime_check;
ALTER TABLE musics DROP CONSTRAINT IF EXISTS musics_year_check;
ALTER TABLE musics DROP CONSTRAINT IF EXISTS genres_length_check;