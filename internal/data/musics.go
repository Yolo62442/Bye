package data
import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"girls/internal/validator"
	"github.com/lib/pq"
	"time"
)
type Music struct {
	ID int64 `json:"id"`
	CreatedAt time.Time `json:"-"`
	Title string `json:"title"`
	Singer string `json:"singer"`
	Year int32 `json:"year,omitempty"`
	// Use the Runtime type instead of int32. Note that the omitempty directive will
	// still work on this: if the Runtime field has the underlying value 0, then it will
	// be considered empty and omitted -- and the MarshalJSON() method we just made
	// won't be called at all.
	Runtime Runtime `json:"runtime,omitempty"`
	Genres []string `json:"genres,omitempty"`
	Version int32 `json:"version"`
}

func ValidateMusic(v *validator.Validator, music *Music) {
	v.Check(music.Title != "", "title", "must be provided")
	v.Check(len(music.Title) <= 500, "title", "must not be more than 500 bytes long")
	v.Check(music.Singer != "", "singer", "must be provided")
	v.Check(len(music.Singer) <= 500, "singer", "must not be more than 500 bytes long")
	v.Check(music.Year != 0, "year", "must be provided")
	v.Check(music.Year >= 1888, "year", "must be greater than 1888")
	v.Check(music.Year <= int32(time.Now().Year()), "year", "must not be in the future")
	v.Check(music.Runtime != 0, "runtime", "must be provided")
	v.Check(music.Runtime > 0, "runtime", "must be a positive integer")
	v.Check(music.Genres != nil, "genres", "must be provided")
	v.Check(len(music.Genres) >= 1, "genres", "must contain at least 1 genre")
	v.Check(len(music.Genres) <= 5, "genres", "must not contain more than 5 genres")
	v.Check(validator.Unique(music.Genres), "genres", "must not contain duplicate values")
}

type MusicModel struct {
	DB *sql.DB
}
// Add a placeholder method for inserting a new record in the musics table.
func (m MusicModel) Insert(music *Music) error {
	query := `
INSERT INTO musics (title, singer, year, runtime, genres)
VALUES ($1, $2, $3, $4, $5)
RETURNING id, created_at, version`
	args := []interface{}{music.Title, music.Singer, music.Year, music.Runtime, pq.Array(music.Genres)}
	// Create a context with a 3-second timeout.// Create a context with a 3-second timeout.
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	// Use QueryRowContext() and pass the context as the first argument.
	return m.DB.QueryRowContext(ctx, query, args...).Scan(&music.ID, &music.CreatedAt, &music.Version)
}
func (m MusicModel) Get(id int64) (*Music, error) {
	if id < 1 {
		return nil, ErrRecordNotFound
	}
	// Remove the pg_sleep(10) clause.
	query := `
SELECT id, created_at, title, singer, year, runtime, genres, version
FROM musics
WHERE id = $1`
	var music Music
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	// Remove &[]byte{} from the first Scan() destination.
	err := m.DB.QueryRowContext(ctx, query, id).Scan(
		&music.ID,
		&music.CreatedAt,
		&music.Title,
		&music.Singer,
		&music.Year,
		&music.Runtime,
		pq.Array(&music.Genres),
		&music.Version,
	)
	if err != nil {
		switch {
		case errors.Is(err, sql.ErrNoRows):
			return nil, ErrRecordNotFound
		default:
			return nil, err
		}
	}
	return &music, nil
}
func (m MusicModel) Update(music *Music) error {
	query := `
UPDATE musics
SET title = $1, singer = $2 year = $3, runtime = $4, genres = $5, version = version + 1
WHERE id = $6 AND version = $7
RETURNING version`
	args := []interface{}{
		music.Title,
		music.Singer,
		music.Year,
		music.Runtime,
		pq.Array(music.Genres),
		music.ID,
		music.Version,
	}
	// Create a context with a 3-second timeout.
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	// Use QueryRowContext() and pass the context as the first argument.// Use QueryRowContext() and pass the context as the first argument.
	err := m.DB.QueryRowContext(ctx, query, args...).Scan(&music.Version)
	if err != nil {
		switch {
		case errors.Is(err, sql.ErrNoRows):
			return ErrEditConflict
		default:
			return err
		}
	}
	return nil
}
func (m MusicModel) Delete(id int64) error {
	if id < 1 {
		return ErrRecordNotFound
	}
	query := `
DELETE FROM musics
WHERE id = $1`
	// Create a context with a 3-second timeout.
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	// Use ExecContext() and pass the context as the first argument.
	result, err := m.DB.ExecContext(ctx, query, id)
	if err != nil {
		return err
	}
	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return err
	}
	if rowsAffected == 0 {
		return ErrRecordNotFound
	}
	return nil
}
func (m MusicModel) GetAll(title string, genres []string, filters Filters) ([]*Music, Metadata, error) {
	query := fmt.Sprintf(`
SELECT count(*) OVER(), id, created_at, title, singer, year, runtime, genres, version
FROM musics
WHERE (to_tsvector('simple', title) @@ plainto_tsquery('simple', $1) OR $1 = '')
AND (genres @> $2 OR $2 = '{}')
ORDER BY %s %s, id ASC
LIMIT $3 OFFSET $4`, filters.sortColumn(), filters.sortDirection())
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	args := []interface{}{title, pq.Array(genres), filters.limit(), filters.offset()}
	rows, err := m.DB.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, Metadata{}, err // Update this to return an empty Metadata struct.
	}
	defer rows.Close()
	// Declare a totalRecords variable.
	totalRecords := 0
	musics := []*Music{}
	for rows.Next() {
		var music Music
		err := rows.Scan(
			&totalRecords, // Scan the count from the window function into totalRecords.
			&music.ID,
			&music.CreatedAt,
			&music.Title,
			&music.Singer,
			&music.Year,
			&music.Runtime,
			pq.Array(&music.Genres),
			&music.Version,
		)
		if err != nil {
			return nil, Metadata{}, err // Update this to return an empty Metadata struct.
		}
		musics = append(musics, &music)
	}
	if err = rows.Err(); err != nil {
		return nil, Metadata{}, err // Update this to return an empty Metadata struct.
	}
	// Generate a Metadata struct, passing in the total record count and pagination
	// parameters from the client.
	metadata := calculateMetadata(totalRecords, filters.Page, filters.PageSize)
	// Include the metadata struct when returning.
	return musics, metadata, nil
}